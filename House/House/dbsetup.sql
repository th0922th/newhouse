-- MySQL dump 10.13  Distrib 5.6.23, for Linux (x86_64)
--
-- Host: localhost    Database: test2
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `test2`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `House` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `House`;

--
-- Table structure for table `Chore`
--

DROP TABLE IF EXISTS `Chore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chore` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `HouseHoldId` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `PayRateLow` decimal(15,2) DEFAULT NULL,
  `PayRateHigh` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Chore_HouseHold` (`HouseHoldId`),
  CONSTRAINT `FK_Chore_HouseHold` FOREIGN KEY (`HouseHoldId`) REFERENCES `HouseHold` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Chore`
--

LOCK TABLES `Chore` WRITE;
/*!40000 ALTER TABLE `Chore` DISABLE KEYS */;
INSERT INTO `Chore` VALUES (1,1,'Clean Room','Clean Everything',0.25,1.00);
/*!40000 ALTER TABLE `Chore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HouseHold`
--

DROP TABLE IF EXISTS `HouseHold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HouseHold` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `HouseName` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HouseHold`
--

LOCK TABLES `HouseHold` WRITE;
/*!40000 ALTER TABLE `HouseHold` DISABLE KEYS */;
INSERT INTO `HouseHold` VALUES (1,'TestHouse');
/*!40000 ALTER TABLE `HouseHold` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Member`
--

DROP TABLE IF EXISTS `Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MemberTypeId` int(11) NOT NULL,
  `HouseHoldId` int(11) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Password` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Member_MemberType` (`MemberTypeId`),
  KEY `FK_Member_HouseHold` (`HouseHoldId`),
  CONSTRAINT `FK_Member_HouseHold` FOREIGN KEY (`HouseHoldId`) REFERENCES `HouseHold` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Member_MemberType` FOREIGN KEY (`MemberTypeId`) REFERENCES `MemberType` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Member`
--

LOCK TABLES `Member` WRITE;
/*!40000 ALTER TABLE `Member` DISABLE KEYS */;
INSERT INTO `Member` VALUES (2,1,1,'Taylor','taylor'),(3,1,1,'Sophia','sophia');
/*!40000 ALTER TABLE `Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MemberType`
--

DROP TABLE IF EXISTS `MemberType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberType` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MemberType` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MemberType`
--

LOCK TABLES `MemberType` WRITE;
/*!40000 ALTER TABLE `MemberType` DISABLE KEYS */;
INSERT INTO `MemberType` VALUES (1,'Parent'),(2,'Child');
/*!40000 ALTER TABLE `MemberType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Member_Chore`
--

DROP TABLE IF EXISTS `Member_Chore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member_Chore` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MemberId` int(11) NOT NULL,
  `ChoreId` int(11) NOT NULL,
  `DateAssigned` datetime NOT NULL,
  `DateCompleted` datetime DEFAULT NULL,
  `CompleteCheck` tinyint(1) NOT NULL,
  `FinalEarning` decimal(15,2) DEFAULT NULL,
  `ConfirmedCheck` tinyint(1) NOT NULL,
  `PaidDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MemberChore_Member` (`MemberId`),
  KEY `FK_MemberChore_Chore` (`ChoreId`),
  CONSTRAINT `FK_MemberChore_Chore` FOREIGN KEY (`ChoreId`) REFERENCES `Chore` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_MemberChore_Member` FOREIGN KEY (`MemberId`) REFERENCES `Member` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Member_Chore`
--

LOCK TABLES `Member_Chore` WRITE;
/*!40000 ALTER TABLE `Member_Chore` DISABLE KEYS */;
INSERT INTO `Member_Chore` VALUES (1,2,1,'2015-03-25 00:00:00',NULL,0,0.00,0,NULL);
/*!40000 ALTER TABLE `Member_Chore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'testy','test-master'),(3,'taylor','taylor_Web'),(11,'Blah','Blah_Web');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-26  8:23:38
