/**
 * Created by taylor.hakes on 7/1/14.
 */
'use strict';
define([
    'marionette',
    'apps/config/marionette/regions/dialog',
    'bootstrap'
], function (Marionette) {
    var HouseHold = new Marionette.Application();
    HouseHold.addRegions({
        headerRegion: '#header-region',
        mainRegion: '#main-region',
        dialogRegion: Marionette.Region.Dialog.extend({ el: '#modal-region' }),
        footerRegion: '#footer-region'
    });
    HouseHold.navigate = function (route, options) {
        options || (options = {});
        Backbone.history.navigate(route, options);
    };
    HouseHold.getCurrentRoute = function () {
        return Backbone.history.fragment;
    };
    HouseHold.startSubApp = function (appName, args) {
        var currentApp = HouseHold.module(appName);
        if (HouseHold.currentApp === currentApp) {
            return;
        }
        if (HouseHold.currentApp) {
            HouseHold.currentApp.stop();
        }
        HouseHold.currentApp = currentApp;
        currentApp.start(args);
    };
    HouseHold.on('start', function () {
        if (Backbone.history) {
            require(['apps/login/login_app',
                    'apps/parent/parent_app',
                    'apps/child/child_app'], function () {
                Backbone.history.start();
                if (HouseHold.getCurrentRoute() === '') {
                    HouseHold.trigger('login');
                }
            });
        }
    });
    return HouseHold;
});