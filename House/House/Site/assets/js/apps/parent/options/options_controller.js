/**
 * Created by taylor.hakes on 8/7/2014.
 */

'use strict';
define([
    'app',
    'apps/parent/options/options_view',
    'entities/parent'
], function (HouseHold, View) {
    HouseHold.module('House.Parent.Options', function (Options, HouseHold, Backbone, Marionette, $, _) {
        Options.Controller = {
            parentOptions: function () {
                require(['entities/parent'], function () {
                    var parentOptionView = new View.ParentOption(),
                        parent;

                    HouseHold.request('get:user', function (user) {
                        if (user.attributes.ValidUser === true) {
                            parent = user;
                            parentOptionView.model = parent;
                            HouseHold.mainRegion.show(parentOptionView);
                        }
                        else {
                            HouseHold.trigger('login');
                        }
                    });

                    parentOptionView.on('member:click', function () {
                        HouseHold.trigger('get:parent:members', parent.attributes);
                    });

                    parentOptionView.on('chore:click', function () {
                        HouseHold.trigger('get:parent:chores', parent.attributes);
                    });
                });
            }
        };
    });
    return HouseHold.House.Parent.Options.Controller;
});