/**
 * Created by taylor.hakes on 8/7/2014.
 */
'use strict';
define([
    'app',
    'tpl!apps/parent/options/templates/parent-options.tpl'
], function (HouseHold, parentOptionTpl) {
    HouseHold.module('Parent.Options.View', function (View, HouseHold, Backbone, Marionette, $, _) {
        View.ParentOption = Marionette.ItemView.extend({
            template: parentOptionTpl,
            className: 'well parent-option',
            triggers: {
                'click .js-member-button': 'member:click',
                'click .js-chore-button': 'chore:click'
            }
        });
    });
    return HouseHold.Parent.Options.View;
});