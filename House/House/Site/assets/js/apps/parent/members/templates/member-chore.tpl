<td class="col-xs-2"><%= Name %></td>
<td class="col-xs-3"><%= Description %></td>
<td class="col-xs-1"><%= DateAssigned %></td>
<td class="currency col-xs-1"><%= PayRateLow %></td>
<td class="currency col-xs-1"><%= PayRateHigh %></td>
<td class="col-xs-2">
    <input type="text" class="currency col-xs-12 earnings"
           name="FinalEarnings" id="final-earning" value="<%= FinalEarning %>"/>
</td>
<td class="col-xs-1 text-center">
	<input type="checkbox" class="checkbox"/>
</td>
<td class="col-xs-1 text-center">
    <a class="js-delete member-hover">
        <span class="glyphicon glyphicon-remove text-danger"></span>
    </a>
</td>