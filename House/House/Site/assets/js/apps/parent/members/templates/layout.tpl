<div id="member-container" class="col-xs-3">
    <div id="member-control"></div>
    <div id="member-region"></div>
</div>
<div id="chores-container" class="col-xs-9">
    <div id="member-chores-control" class="col-xs-12"></div>
    <div id="member-chores-done-region"></div>
    <div id="member-chores-region"></div>
</div>