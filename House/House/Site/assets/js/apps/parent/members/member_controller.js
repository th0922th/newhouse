/**
 * Created by taylor.hakes on 7/2/14.
 */
'use strict';
define([
    'app',
    'apps/parent/members/member_view',
    'common/views',
    'entities/parent',
    'backbone.stickit'
], function (HouseHold, View, CommonView) {
    HouseHold.module('House.Parent.Member', function (Member, HouseHold, Backbone, Marionette, $, _) {
        Member.Controller = {

            showMembers: function (user) {
                require(['entities/parent'], function () {
                    var layoutView = new View.Layout(),
                        modalViewLayout = new CommonView.ModalLayout(),
                        resView = new CommonView.Response(),
                        memberControlView = new View.MemberControl(),
                        newMemberModel = HouseHold.request('new:member'),
                        newMemberView = new View.NewMember({model: newMemberModel}),
                        deleteMemberFooterView,
                        membersView = new View.Members();

                    layoutView.on('show', function () {
                        if (user.ValidUser === true) {
                            layoutView.memberControl.show(memberControlView);
                            HouseHold.request('get:parent:members', user, function (members) {
                                membersView.collection = members;
                                layoutView.memberRegion.show(membersView);
                            });
                        }
                        else {
                            HouseHold.trigger('login');
                        }
                    });

                    memberControlView.on('add:member', function () {
                        //Display Modal
                        modalViewLayout.ModalHeader = "Add New Member";
                        var newMemberFooterView = new View.MemberFooter();
                        newMemberFooterView.Action = "Save";
                        HouseHold.dialogRegion.show(modalViewLayout);
                        modalViewLayout.centerRegion.show(newMemberView);
                        modalViewLayout.footerRegion.show(newMemberFooterView);

                        //Modals save event
                        newMemberFooterView.on('save:member', function () {
                            var data = Backbone.Syphon.serialize(newMemberView);
                            data.HouseHoldId = user.HouseHoldId;

                            //Send form data to service
                            HouseHold.request('save:member', data, function (response) {
                                //Display response modal
                                modalViewLayout.ModalHeader = "Save Member";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                resView.model = response;
                                resView.model.ResLabel = "Creating a new member success was ";
                                modalViewLayout.centerRegion.show(resView);

                                //Update the member list
                                if (response.get("id") > 0) {
                                    layoutView.trigger('show');
                                }
                            });

                        });

                    });

                    membersView.on("childview:delete:member", function (member) {
                        //Setup Delete confirmation dialog
                        modalViewLayout.ModalHeader = "Delete Member";
                        deleteMemberFooterView = new View.DeleteMemberFooter();
                        deleteMemberFooterView.Action = "Delete";
                        HouseHold.dialogRegion.show(modalViewLayout);
                        resView.model = new Backbone.Model({Success: "", Message: "Please don't delete me."});
                        resView.model.ResLabel = "Are you sure you want to delete member " + member.model.attributes.Name + "?";
                        modalViewLayout.centerRegion.show(resView);
                        modalViewLayout.footerRegion.show(deleteMemberFooterView);

                        deleteMemberFooterView.on('delete:member', function () {
                            HouseHold.request('delete:member', member.model.attributes.Id, function (response) {
                                //Setup Delete response dialog.
                                modalViewLayout.ModalHeader = "Delete Member";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                resView.model = response;
                                resView.model.ResLabel = "Delete action success was ";
                                modalViewLayout.centerRegion.show(resView);

                                //Update the member list
                                if (response.attributes.Success === true) {
                                    member.model.destroy();
                                }
                            });
                        });
                    });

                    membersView.on('childview:edit:member', function (member) {
                        //Display Modal
                        modalViewLayout.ModalHeader = "Edit Member";
                        var editMemberFooterView = new View.MemberFooter();
                        editMemberFooterView.Action = "Save";
                        HouseHold.dialogRegion.show(modalViewLayout);
                        var editMemberView = new View.NewMember({model: member.model});
                        modalViewLayout.centerRegion.show(editMemberView);
                        modalViewLayout.footerRegion.show(editMemberFooterView);

                        //Modals save event
                        editMemberFooterView.on('save:member', function () {
//                            var data = Backbone.Syphon.serialize(editMemberView);
//                            data.Id = member.model.attributes.Id;
//                            data.HouseHoldId = user.HouseHoldId;

                            //Send form data to service
                            HouseHold.request('update:member', editMemberView.model, function (response) {
                                //Display response modal
                                modalViewLayout.ModalHeader = "Save Member";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                var resView = new CommonView.Response({model: response});
                                resView.model.ResLabel = "Edit member success was ";
                                modalViewLayout.centerRegion.show(resView);

                                //Update the member list
                                if (response.get("Success") === true) {
                                    layoutView.trigger('show');
                                }
                            });
                        });
                    });

                    membersView.on("childview:show:member:chores", function (member) {
                        Member.Controller.showMemberChores(layoutView, user, member);
                    });

                    HouseHold.mainRegion.show(layoutView);
                });
            },

            showMemberChores: function (layoutView, parent, member) {
                var memberChoresDoneView = new View.MemberChores(),
                    memberChoresView = new View.MemberChores(),
                    memChoreControl = new View.MemChoreControl({model: {Name: member.model.get('Name'), Total: 0}}),
                    modalViewLayout = new CommonView.ModalLayout();

                HouseHold.request('get:member:chores', member.model.attributes.Id, true, function (doneChores) {
                    var memChoreLayoutDone = new View.MemChoreLayout({model: {Title: "Chores Claimed To Be Completed"}});
                    memberChoresDoneView.collection = doneChores;
                    _.each(doneChores.models, function (value, key, all) {
                        if(value.attributes.ConfirmedCheck === true){
                            var memChoreModel = memChoreControl.model;
                            memChoreModel.set('Total', memChoreModel.get('Total') + value.get('FinalEarning'));
                        }
                    });
                    layoutView.memberChoreControl.show(memChoreControl);
                    layoutView.memberChoresDoneRegion.show(memChoreLayoutDone);
                    memChoreLayoutDone.memChores.show(memberChoresDoneView);

                    HouseHold.request('get:member:chores', member.model.attributes.Id, false, function (chores) {
                        var memChoreLayout = new View.MemChoreLayout({model: {Title: "Chores Assigned"}});
                        memberChoresView.collection = chores;
                        layoutView.memberChoresRegion.show(memChoreLayout);
                        memChoreLayout.memChores.show(memberChoresView);
                    });
                });

                memberChoresDoneView.on('childview:update:total', function(){
                    var totalEarnings = 0;
                    _.each(memberChoresDoneView.collection.models, function(value, key, all){
                        if(value.attributes.ConfirmedCheck === true){
                            totalEarnings += parseFloat(parseFloat(value.attributes.FinalEarning.replace('$', '')).toFixed(2));
                        }
                    });
                    memChoreControl.model.Total = totalEarnings;
                    memChoreControl.render();
                });

                memberChoresView.on('childview:delete:member:chore', function(view, model){
                    var resView = new CommonView.Response(),
                        deleteMemberFooterView = new View.DeleteMemberFooter();
                    //Setup Delete confirmation dialog
                    modalViewLayout.ModalHeader = "Delete Member Chore";

                    deleteMemberFooterView.Action = "Delete";
                    HouseHold.dialogRegion.show(modalViewLayout);
                    resView.model = new Backbone.Model({Success: "", Message: "Please don't delete me."});
                    resView.model.ResLabel = "Are you sure you want to delete this members chore?";
                    modalViewLayout.centerRegion.show(resView);
                    modalViewLayout.footerRegion.show(deleteMemberFooterView);

                    deleteMemberFooterView.on('delete:member', function () {
                        HouseHold.request('delete:member:chore', model.attributes.Id, function (response) {
                            //Setup Delete response dialog.
                            modalViewLayout.ModalHeader = "Deleted Members Chore";
                            HouseHold.dialogRegion.show(modalViewLayout);
                            resView.model = response;
                            resView.model.ResLabel = "Delete action success was ";
                            modalViewLayout.centerRegion.show(resView);

                            //Update the member list
                            if (response.attributes.Success === true) {
                                view.model.destroy();
                            }
                        });
                    });
                });

                memChoreControl.on('save:chores:status', function(){
                    var chores = {"MemberChoresReq": []};
                    _.each(memberChoresDoneView.collection.models, function(value, key, all){
                        chores.MemberChoresReq.push(value.attributes);
                    });
                    _.each(memberChoresView.collection.models, function(value, key, all){
                        chores.MemberChoresReq.push(value.attributes);
                    });

                    HouseHold.request('save:member:chores', chores, function (res){
                        if(res.attributes[0].Success === true){
                            //Display Modal
                            modalViewLayout.ModalHeader = "Save";
                            HouseHold.dialogRegion.show(modalViewLayout);
                            var resView = new CommonView.Response({model: new Backbone.Model({Message: ""})});
                            resView.model.ResLabel = "Save was Successful";
                            modalViewLayout.centerRegion.show(resView);
                        }
                    });
                });

                memChoreControl.on('pay:chores', function(){
                    var chores = {"MemberChoresReq": []};
                    _.each(memberChoresDoneView.collection.models, function(value, key, all){
                        if(value.attributes.ConfirmedCheck === true){
                            var now = new Date();
                            value.attributes.PaidDate = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
                            value.attributes.FinalEarning = value.attributes.FinalEarning.replace('$', '');
                            chores.MemberChoresReq.push(value.attributes);
                        }

                    });

                    HouseHold.request('save:member:chores', chores, function (res){
                        if(res.attributes[0].Success === true){
                            _.each(memberChoresDoneView.collection.models, function(value, key, all){
                                if(value.attributes.ConfirmedCheck === true){
                                    memberChoresDoneView.collection.models[key].destroy();
                                }
                            });
                        }
                    });
                });
            }
        };
    });
    return HouseHold.House.Parent.Member.Controller;
});