<form class="form-horizontal" role="form">
    <div class="form-group">
        <label for="Name" class="col-xs-4 text-right">Name</label>

        <div class="col-xs-8">
            <input type="text" class="form-control" name="Name" id="Name" placeholder="Name"/>
        </div>
    </div>
    <div class="form-group">
        <label for="Password" class="col-xs-4 text-right">Password</label>

        <div class="col-xs-8">
            <input type="password" class="form-control" name="Password" id="Password" placeholder="Password"/>
        </div>
    </div>
    <div class="form-group">
        <label for="Confirm-Password" class="col-xs-4 text-right">Password</label>

        <div class="col-xs-8">
            <input type="password" class="form-control" name="Confirm-Password" id="Confirm-Password" placeholder="Confirm Password"/>
        </div>
    </div>
    <div class="form-group">
        <label for="MemberTypeId" class="col-xs-4 text-right">Member Type</label>

        <div class="col-xs-8">
            <select class="form-control" id="MemberTypeId" name="MemberTypeId">
                <option value="2">House Member</option>
                <option value="1">House Parent</option>
            </select>
        </div>
    </div>
</form>