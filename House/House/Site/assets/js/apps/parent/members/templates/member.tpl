<td class="js-member-row member-hover col-xs-6" id="blah"></td>
<td class="col-xs-4"></td>
<td class="col-xs-1 member-hover">
    <a class="js-edit">
        <span class="glyphicon glyphicon-pencil text-warning"></span>
    </a>
</td>
<td class="col-xs-1 member-hover">
    <a class="js-delete">
        <span class="glyphicon glyphicon-remove text-danger"></span>
    </a>
</td>