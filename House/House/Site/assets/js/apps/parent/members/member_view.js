/**
 * Created by taylor.hakes on 7/6/14.
 */
'use strict';
define([
    'app',
    'common/views',
    'tpl!apps/parent/members/templates/layout.tpl',
    'tpl!apps/parent/members/templates/member-control.tpl',
    'tpl!apps/parent/members/templates/new-member-form.tpl',
    'tpl!apps/parent/members/templates/member.tpl',
    'tpl!apps/parent/members/templates/members.tpl',
    'tpl!apps/parent/members/templates/mem-chore-layout.tpl',
    'tpl!apps/parent/members/templates/member-chore.tpl',
    'tpl!apps/parent/members/templates/member-chores.tpl',
    'tpl!apps/parent/members/templates/mem-chore-control.tpl',
    "common/helpers",
    'common/extensions/number',
    'backbone.stickit',
    'backbone.syphon',
    "bootstrap-checkbox",
    'bootstrap-select'
], function (HouseHold, CommonViews, layoutTpl, memberControlTpl,
             newMemberFormTpl, memberTpl, membersTpl, memChoreLayout, memberChoreTpl, memberChoresTpl,
             memChoreControlTpl) {
    HouseHold.module('Parent.Member.View', function (View, HouseHold, Backbone, Marionette, $, _) {
        View.Layout = Marionette.LayoutView.extend({
            template: layoutTpl,
            className: "row",
            regions: {
                memberControl: '#member-control',
                memberRegion: '#member-region',
                memberChoreControl: '#member-chores-control',
                memberChoresDoneRegion: '#member-chores-done-region',
                memberChoresRegion: '#member-chores-region'
            }
        });

        View.MemberControl = Marionette.ItemView.extend({
            template: memberControlTpl,
            triggers: {
                'click .js-add-member': 'add:member'
            }
        });

        View.NewMember = Marionette.ItemView.extend({
            template: newMemberFormTpl,
            className: "form-horizontal",
            bindings: {
                "#Name": "Name",
                "#Password": "Password",
                "#Confirm-Password": "Confirm-Password",
                "#MemberTypeId": "MemberTypeId"
            },
            onRender: function(){
                this.stickit();
            }
        });

        View.MemberFooter = CommonViews.Footer.extend({
            triggers: {
                "click .js-action": "save:member"
            }
        });

        View.DeleteMemberFooter = CommonViews.Footer.extend({
            triggers: {
                "click .js-action": "delete:member"
            }
        });

        View.AMember = Marionette.ItemView.extend({
            template: memberTpl,
            tagName: 'tr',
            bindings: {
                "#blah": "Name"
            },
            events: {
                "click .js-edit": "editClicked",
                "click .js-delete": "deleteClicked",
                "click .js-member-row": "rowClicked"
            },
            editClicked: function(e){
                e.preventDefault();
                this.trigger("edit:member", this.model);
            },
            deleteClicked: function(e){
                e.preventDefault();
                this.trigger("delete:member", this.model);
            },
            rowClicked: function(e){
                e.preventDefault();
                this.trigger("show:member:chores", this);
            },
            onRender: function(){
                this.stickit();
            }
        });

        View.Members = Marionette.CompositeView.extend({
            tagName: 'table',
            className: 'table table-hover table-striped',
            childView: View.AMember,
            childViewContainer: 'tbody',
            template: membersTpl
        });

        View.MemChoreLayout = Marionette.LayoutView.extend({
            tagName: "table",
            className: "table",
            template: memChoreLayout,
            bindings: {
                '#Title': 'Title'
            },
            childViewContainer: "tbody",
            regions: {
                memChores: ".inner-table-container"
            },
            onRender: function(){
                this.stickit();
            }
        });

        View.MemberChore = Marionette.ItemView.extend({
            template: memberChoreTpl,
            tagName: 'tr',
            className: 'row',
            bindings: {
                '#final-earning': 'FinalEarning',
                '.checkbox': 'ConfirmedCheck'
            },
            events: {
                "click .js-update-total": "updateTotal",
                "click .js-delete": "deleteMemChore",
                "focusout input.earnings": "earningFocusOut"
            },
            updateTotal: function(e){
                e.preventDefault();
                this.trigger('update:total', this.model);
            },
            deleteMemChore: function(e){
                e.preventDefault();
                this.trigger("delete:member:chore", this.model);
            },
            earningFocusOut: function () {
                this.$el.find('.earnings').val(this.$el.find('.earnings').val().replace(/[^0-9\.]/g, ''));
                this.model.set('FinalEarning', this.model.get('FinalEarning').formatMoney('$'));
                this.trigger('update:total', this.model);
            },
            onRender: function(){
                this.stickit();
                this.model.set('FinalEarning', this.model.get('FinalEarning').formatMoney('$'));
            },
            onBeforeRender: function(){
                var pDate = new Date(parseInt(this.model.attributes.DateAssigned.substring(6)));
                this.model.attributes.DateAssigned = new String(pDate).toShortDate();
            },
            onShow: function(){
                this.$el.find('.checkbox').checkbox({
                    buttonStyle: 'btn-danger js-update-total',
                    buttonStyleChecked: 'btn-success js-update-total',
                    checkedClass: 'cb-icon-check',
                    uncheckedClass: 'cb-icon-check-empty'
                });
            }
        });

        View.MemberChores = Marionette.CompositeView.extend({
            template: memberChoresTpl,
            tagName: 'table',
            className: 'table table-hover table-striped chore-table',
            childView: View.MemberChore,
            childViewContainer: 'tbody'
        });

        View.MemChoreControl = Marionette.ItemView.extend({
            template: memChoreControlTpl,
            bindings: {
                '#Name': 'Name',
                '#Total': 'Total'
            },
            events: {
                "click .js-save-chores": "saveChores",
                "click .js-pay-chores": "payChores"
            },
            saveChores: function(e){
                e.preventDefault();
                this.trigger("save:chores:status");
            },
            payChores: function(e){
                e.preventDefault();
                this.trigger("pay:chores");
            },
            onRender: function(){
                this.stickit();
                this.model.set('Total', this.model.get('Total').formatMoney('$'));
            }
        });

    });
    return HouseHold.Parent.Member.View;
});