/**
 * Created by taylor.hakes on 7/6/14.
 */
'use strict';
define(['app'], function (HouseHold) {
    HouseHold.module('Parent', function (Parent) {
        Parent.startWithParent = false;
        Parent.onStart = function () {
            console.log('Starting Parent App.');
        };
        Parent.onStop = function () {
            console.log('Stopping Parent App.');
        };
    });
    HouseHold.module('Routers.Parent', function (ParentRouter, HouseHold, Backbone, Marionette, $, _) {
        ParentRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                'parent': 'parent',
                'parent/members': 'parentMembers',
                'parent/chores': 'parentChores'
            }
        });
        var API = {
            parent: function () {
                require([
                    'apps/parent/options/options_controller',
                    'entities/parent'
                ], function (OptionController) {
                    HouseHold.startSubApp('parent');
                    OptionController.parentOptions();
                });
            },
            parentMembers: function (parent) {
                require([
                    'apps/parent/members/member_controller',
                    'entities/parent'
                ], function (MemberController) {
                    if (parent != undefined) {
                        MemberController.showMembers(parent);
                    }
                    else {
                        HouseHold.request('get:user', function (user) {
                            if (user.attributes.ValidUser === true) {
                                MemberController.showMembers(user.attributes);
                            }
                            else {
                                HouseHold.trigger('login');
                            }
                        });
                    }
                });
            },
            parentChores: function(parent){
                require([
                    'apps/parent/chores/chores_controller',
                    'entities/parent'
                ], function (ChoresController) {
                    if (parent != undefined) {
                        ChoresController.showChores(parent);
                    }
                    else {
                        HouseHold.request('get:user', function (user) {
                            if (user.attributes.ValidUser === true) {
                                ChoresController.showChores(user.attributes);
                            }
                            else {
                                HouseHold.trigger('login');
                            }
                        });
                    }
                });
            }
        };
        HouseHold.on('parent:house', function () {
            HouseHold.navigate('parent');
            API.parent();
        });

        HouseHold.on('get:parent:members', function (parent) {
            HouseHold.navigate('parent/members');
            API.parentMembers(parent);
        });

        HouseHold.on('get:parent:chores', function(parent){
            HouseHold.navigate('parent/chores');
            API.parentChores(parent);
        });

        HouseHold.addInitializer(function () {
            new ParentRouter.Router({ controller: API });
        });
    });
    return HouseHold.Parent;
});