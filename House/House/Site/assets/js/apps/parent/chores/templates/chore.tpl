<td class="col-xs-2" id="Name"></td>
<td class="col-xs-3 chore-table-item" id="Description"></td>
<td class="col-xs-1" id="PayRateLow"></td>
<td class="col-xs-1" id="PayRateHigh"></td>
<td class="col-xs-2">
    <div id="select-region"></div>
</td>
<td class="col-xs-1">
    <input class="date-assigned form-control" name="DateAssigned" type="date" value="<%= DateAssigned %>"
           placeholder="Assign Date"/>
</td>
<td class="col-xs-1 chore-table-item">
    <button class="btn btn-small btn-default js-assign">Assign</button>
</td>
<td class="col-xs-1">
    <a class="js-edit member-hover pull-left icon-padding">
        <span class="glyphicon glyphicon-pencil text-warning"></span>
    </a>
    <a class="js-delete member-hover pull-right icon-padding">
        <span class="glyphicon glyphicon-remove text-danger"></span>
    </a>
</td>