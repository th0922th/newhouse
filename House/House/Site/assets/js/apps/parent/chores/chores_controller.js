/**
 * Created by taylor.hakes on 8/7/2014.
 */
'use strict';
define([
    'app',
    'apps/parent/chores/chores_view',
    'common/views',
    'entities/parent',
    'bootstrap-select'
], function (HouseHold, View, CommonView) {
    HouseHold.module('House.Parent.Chores', function (Chores, HouseHold, Backbone, Marionette, $, _) {
        Chores.Controller = {
            showChores: function (user) {
                require(['entities/parent'], function () {
                    var choreLayoutView = new View.ChoreLayout(),
                        choresControlView = new View.ChoresControl(),
                        modalViewLayout = new CommonView.ModalLayout(),
                        resView = new CommonView.Response(),
                        choresView = new View.Chores(),
                        houseMembers;

                    HouseHold.request('get:parent:members', user, function (members) {
                        houseMembers = members;
                        HouseHold.mainRegion.show(choreLayoutView);
                    });

                    choreLayoutView.on('show', function () {
                        if (user.ValidUser === true) {
                            choreLayoutView.choreControlRegion.show(choresControlView);
                            HouseHold.request('get:house:chores', user.HouseHoldId, function(chores){

                                choresView.collection = chores;
                                choreLayoutView.choreMainRegion.show(choresView);
                                _.each(choresView.children._views, function(value, key, all){
                                    var memsView = new View.MemberSelects({collection: houseMembers});
                                    choresView.children._views[key].memberRegion.show(memsView);
                                });
                            });

                        }
                        else {
                            HouseHold.trigger('login');
                        }
                    });

                    choresView.on('childview:assign:chore', function(view, assignMembers){
                        var chores = {"MemberChoresReq": []};
                        _.each(assignMembers, function(value, key, all){
                            var memChore = {"Id":0,"MemberId":0,"ChoreId":0,"DateAssigned": "","CompleteCheck":false,
                                "ConfirmedCheck":false,"DateCompleted":null,"FinalEarning":0,"PaidDate":null};
                            _.each(houseMembers.models, function(v, k, a){
                                if(v.attributes.Name === value){
                                    memChore.MemberId = v.attributes.Id;
                                    memChore.ChoreId = view.model.attributes.Id;
                                    memChore.DateAssigned = view.model.attributes.DateAssigned;
                                    chores.MemberChoresReq.push(memChore);
                                }
                            });
                        });

                        HouseHold.request('save:member:chores', chores, function (res){
                            if(res.attributes[0].Success === true){
                                //Display Modal
                                modalViewLayout.ModalHeader = "Saved";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                var resView = new CommonView.Response({model: new Backbone.Model({Message: ""})});
                                resView.model.ResLabel = "Save was Successful";
                                modalViewLayout.centerRegion.show(resView);
                            }
                        });
                    });

                    choresView.on('childview:edit:chore', function (choreView, chore) {
                        //Display Modal
                        modalViewLayout.ModalHeader = "Edit Chore";
                        var editChoreFooterView = new View.ChoresFooter();
                        editChoreFooterView.Action = "Save";
                        HouseHold.dialogRegion.show(modalViewLayout);
                        var editMemberView = new View.NewChore({model: chore});
                        modalViewLayout.centerRegion.show(editMemberView);
                        modalViewLayout.footerRegion.show(editChoreFooterView);

                        //Modals save event
                        editChoreFooterView.on('chore:action', function () {
                            var formData = Backbone.Syphon.serialize(editMemberView);
                            formData.HouseHoldId = user.HouseHoldId;
                            formData.Id = chore.attributes.Id;

                            //Send form data to service
                            HouseHold.request('save:chore', formData, function (response) {
                                //Display response modal
                                modalViewLayout.ModalHeader = "Saved Chore";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                resView.model = response;
                                resView.model.ResLabel = "Edit chore success was ";
                                modalViewLayout.centerRegion.show(resView);

                                //Update the chore list
                                if (response.attributes.Success === true) {
                                    choreLayoutView.trigger('show');
                                }
                            });
                        });
                    });

                    choresView.on('childview:delete:chore', function(choreView, chore){
                        var resView = new CommonView.Response(),
                            deleteChoreFooterView = new View.ChoresFooter();
                        //Setup Delete confirmation dialog
                        modalViewLayout.ModalHeader = "Delete Chore";

                        deleteChoreFooterView.Action = "Delete";
                        HouseHold.dialogRegion.show(modalViewLayout);
                        resView.model = new Backbone.Model({Success: "", Message: "Please don't delete me."});
                        resView.model.ResLabel = "Are you sure you want to delete this chore?";
                        modalViewLayout.centerRegion.show(resView);
                        modalViewLayout.footerRegion.show(deleteChoreFooterView);

                        deleteChoreFooterView.on('chore:action', function () {
                            HouseHold.request('delete:chore', chore.attributes.Id, function (response) {
                                //Setup Delete response dialog.
                                modalViewLayout.ModalHeader = "Deleted Chore";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                resView.model = response;
                                resView.model.ResLabel = "Delete action success was ";
                                modalViewLayout.centerRegion.show(resView);

                                //Update the member list
                                if (response.attributes.Success === true) {
                                    choreView.model.destroy();
                                }
                            });
                        });
                    });

                    choresControlView.on('add:new:chore', function(){
                        //Display Modal
                        modalViewLayout.ModalHeader = "Add New Chore";
                        var newChoreFooterView = new View.ChoresFooter(),
                            newChoreModel = HouseHold.request('new:chore'),
                            newMemberView = new View.NewChore({model: newChoreModel});
                        newChoreFooterView.Action = "Save";
                        HouseHold.dialogRegion.show(modalViewLayout);
                        modalViewLayout.centerRegion.show(newMemberView);
                        modalViewLayout.footerRegion.show(newChoreFooterView);

                        //Modals save event
                        newChoreFooterView.on('chore:action', function () {
                            var data = Backbone.Syphon.serialize(newMemberView);
                            data.HouseHoldId = user.HouseHoldId;

                            //Send form data to service
                            HouseHold.request('save:chore', data, function (response) {
                                //Display response modal
                                modalViewLayout.ModalHeader = "Saved Chore";
                                HouseHold.dialogRegion.show(modalViewLayout);
                                resView.model = response;
                                resView.model.ResLabel = "Creating a new chore success was ";
                                modalViewLayout.centerRegion.show(resView);

                                //Update the chore list
                                if (response.attributes.Success === true) {
                                    choreLayoutView.trigger('show');
                                }
                            });
                        });
                    });
                });
            }
        };
    });
    return HouseHold.House.Parent.Chores.Controller;
});