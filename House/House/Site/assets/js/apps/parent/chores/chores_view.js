/**
 * Created by taylor.hakes on 8/7/2014.
 */
'use strict';
define([
    'app',
    'common/views',
    'tpl!apps/parent/chores/templates/chore-layout.tpl',
    'tpl!apps/parent/chores/templates/chores-control.tpl',
    'tpl!apps/parent/chores/templates/member-select.tpl',
    'tpl!apps/parent/chores/templates/member-selects.tpl',
    'tpl!apps/parent/chores/templates/chore.tpl',
    'tpl!apps/parent/chores/templates/chores.tpl',
    'tpl!apps/parent/chores/templates/new-chore.tpl',
    "common/helpers",
    'common/extensions/number',
    'backbone.syphon',
    'backbone.stickit',
    "bootstrap-checkbox",
    'bootstrap-select',
    'bootstrap-datetimepicker'
], function (HouseHold, CommonViews, choreLayoutTpl, choresControlTpl, memSelectTpl, memSelectsTpl, choreTpl, choresTpl, newChoreTpl) {
    HouseHold.module('Parent.Chores.View', function (View, HouseHold, Backbone, Marionette, $, _) {

        View.ChoreLayout = Marionette.LayoutView.extend({
            template: choreLayoutTpl,
            regions: {
                choreControlRegion: '#chore-control',
                choreMainRegion: '#chore-main'
            }
        });

        View.ChoresControl = Marionette.ItemView.extend({
            template: choresControlTpl,
            events: {
                "click .js-add-chore": 'addNewChore'
            },
            addNewChore: function(e){
                this.trigger('add:new:chore');
            }
        });

        View.MemberSelect = Marionette.ItemView.extend({
            template: memSelectTpl,
            tagName: 'option'
        });

        View.MemberSelects = Marionette.CompositeView.extend({
            template: memSelectsTpl,
            childView: View.MemberSelect,
            childViewContainer: 'select',
            onShow: function(){
                this.$el.find('.selectpicker').selectpicker();
            }
        });

        View.ChoreSelectLayout = Marionette.LayoutView.extend({
            template: choreTpl,
            tagName: 'tr',
            className: "chore-table-item",
            regions: {
                memberRegion: '#select-region'
            },
            bindings: {
                '#Name': 'Name',
                '#Description': 'Description',
                '#PayRateLow': 'PayRateLow',
                '#PayRateHigh': 'PayRateHigh',
                '.date-assigned': 'DateAssigned'
            },
            events: {
                "click .js-assign": "assignClicked",
                "click .js-edit": "editClicked",
                "click .js-delete": "deleteClicked"
            },
            assignClicked: function(e){
                e.preventDefault();
                var mems = this.$el.find('.member-select').val();
                if(mems.length > 0){
                    this.trigger('assign:chore', mems);
                }
            },
            editClicked: function(e){
                e.preventDefault();
                this.trigger('edit:chore', this.model);
            },
            deleteClicked: function(e){
                e.preventDefault();
                this.trigger('delete:chore', this.model);
            },
            onRender: function(e){
                this.stickit();
                this.model.set('PayRateLow', this.model.get('PayRateLow').formatMoney('$'));
                this.model.set('PayRateHigh', this.model.get('PayRateHigh').formatMoney('$'));
            }
        });

        View.Chores = Marionette.CompositeView.extend({
            template: choresTpl,
            tagName: 'table',
            className: 'table table-hover table-striped table-condensed chore-table col-xs-12',
            childView: View.ChoreSelectLayout,
            childViewContainer: 'tbody'
        });

        View.NewChore = Marionette.ItemView.extend({
            template: newChoreTpl,
            className: "form-horizontal"
        });

        View.ChoresFooter = CommonViews.Footer.extend({
            triggers: {
                "click .js-action": "chore:action"
            }
        });

    });
    return HouseHold.Parent.Chores.View;
});