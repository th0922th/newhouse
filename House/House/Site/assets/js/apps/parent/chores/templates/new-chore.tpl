<form>
    <div class="form-group">
        <label for="Name" class="col-xs-4 text-right">Name</label>

        <div class="col-xs-8">
            <input type="text" class="form-control" name="Name" value="<%= Name %>" id="Name" placeholder="Name"/>
        </div>
    </div>
    <div class="form-group">
        <label for="Description" class="col-xs-4 text-right">Description</label>

        <div class="col-xs-8">
            <input type="text" class="form-control" name="Description" value="<%= Description %>" id="Description" placeholder="Description"/>
        </div>
    </div>
    <div class="form-group">
        <label for="PayRateLow" class="col-xs-4 text-right">Pay Rate Low</label>

        <div class="col-xs-8">
            <input type="text" class="form-control currency" name="PayRateLow" value="<%= PayRateLow %>" id="PayRateLow" placeholder="Pay Rate Low"/>
        </div>
    </div>
    <div class="form-group">
        <label for="PayRateHigh" class="col-xs-4 text-right">Pay Rate High</label>

        <div class="col-xs-8">
            <input type="text" class="form-control currency" name="PayRateHigh" value="<%= PayRateHigh %>" id="PayRateHigh" placeholder="Pay Rate High"/>
        </div>
    </div>
</form>