/**
 * Created by taylor.hakes on 6/12/14.
 */
'use strict';
define(['app'], function (HouseHold) {
    HouseHold.module('Login', function (Login) {
        Login.startWithParent = true;
        Login.onStart = function () {
            console.log('Starting Login App.');
        };
        Login.onStop = function () {
            console.log('Stopping Login App.');
        };
    });
    HouseHold.module('Routers.Login', function (LoginRouter, HouseHold, Backbone, Marionette, $, _) {
        LoginRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                '': 'login',
                'login': 'login'
            }
        });
        var API = {
            login: function () {
                require([
                    'apps/login/login_controller',
                    'entities/login'
                ], function (LoginController) {
                    //StoresPaid.startSubApp(null);
                    LoginController.login();
                });
            },
            logout: function () {
                require([
                    'entities/Login'
                ], function () {
                    HouseHold.request('logout', function () {
                        HouseHold.trigger('login');
                    });
                });
            }
        };
        HouseHold.on('login', function () {
            HouseHold.navigate('login');
            API.login();
        });

        HouseHold.on('logout', function () {
            API.logout();
        });

        HouseHold.addInitializer(function () {
            new LoginRouter.Router({ controller: API });
        });
    });
    return HouseHold.Login;
});
