<form class="form-horizontal">
    <div class="form-group">
        <label for="HouseName" class="col-xs-4 text-right">HouseName</label>

        <div class="col-xs-8">
            <input type="text" class="form-control" name="HouseName" value="<%= HouseName %>" id="HouseName" placeholder="HouseName"/>
        </div>
    </div>
    <div class="form-group">
        <label for="Name" class="col-xs-4 text-right">Name</label>

        <div class="col-xs-8">
            <input type="text" class="form-control" name="Name" value="<%= Name %>" id="Name" placeholder="Name"/>
        </div>
    </div>
    <div class="form-group">
        <label for="Password" class="col-xs-4 text-right">Password</label>

        <div class="col-xs-8">
            <input type="password" class="form-control" name="Password" value="<%= Password %>" id="Password" placeholder="Password"/>
        </div>
    </div>
</form>