<form class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-md-4 text-right" for="input-username">User Name:</label>

        <div class="col-md-4">
            <input id="input-username" name="username" type="text" class="form-control"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 text-right" for="input-password">Password:</label>
        <div class="col-md-4">
            <input id="input-password" name="password" type="password" class="form-control"/>
        </div>
    </div>
    <div class="form-group" style="text-align: right">
        <div class="col-md-4 text-right">
            <button class="btn btn-primary js-submit-button">Login</button>
        </div>
    </div>

</form>