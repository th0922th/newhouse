/**
 * Created by taylor.hakes on 6/12/14.
 */
'use strict';
define([
    'app',
    'apps/login/login_view',
    'common/views',
    'entities/login'
], function (HouseHold, View, CommonView) {
    HouseHold.module('Page.Login', function (Login, HouseHold, Backbone, Marionette, $, _) {
        Login.Controller = {
            login: function () {
                require(['entities/login'], function () {
                    var loginLayout = new View.Layout(),
                        loginModel = HouseHold.request('new:login'),
                        formView = new View.LoginForm({
                            model: loginModel
                        }),
                        loginControl = new View.LoginControl(),
                        newHouseModel = HouseHold.request('new:house'),
                        newHouseView = new View.NewHouse({model: newHouseModel}),
                        //modalViewLayout = new CommonView.ModalLayout(),
                        //resView = new CommonView.Response(),
                        errorView = new View.ErrorResponse();

                    loginLayout.on('show', function () {
                        $(".js-logout").hide();
                        loginLayout.formRegion.show(formView);
                        loginLayout.controlRegion.show(loginControl);
                    });

                    formView.on('submit', function (userInfo) {
                        HouseHold.request('validate:login', userInfo, function (responseModel) {
                            if (responseModel.attributes.ValidUser === true) {
                                $(".js-logout").show();
                                require(['apps/parent/parent_app', 'apps/child/child_app'], function(){
                                    if (responseModel.attributes.MemberTypeId === 1) {
                                        HouseHold.trigger("parent:house");
                                    }
                                    else {
                                        HouseHold.trigger("child:house", responseModel.attributes);
                                    }
                                });
                            }
                            else {
                                errorView.ErrorMessage = "Invalid User.";
                                loginLayout.errorRegion.show(errorView);
                            }
                        });
                    });
//
//                    loginControl.on('new:house', function(){
//                        //Display Modal
//                        modalViewLayout.ModalHeader = "Create New House";
//                        var newHouseFooterView = new View.LoginFooter();
//                        newHouseFooterView.Action = "Save";
//                        HouseHold.dialogRegion.show(modalViewLayout);
//                        modalViewLayout.centerRegion.show(newHouseView);
//                        modalViewLayout.footerRegion.show(newHouseFooterView);
//
//                        //Modals save event
//                        newHouseFooterView.on('login:action', function () {
//                            var data = Backbone.Syphon.serialize(newHouseView);
//
//                            //Send form data to service
//                            HouseHold.request('save:new:house', data, function (response) {
//                                //Display response modal
//                                modalViewLayout.ModalHeader = "Saved House";
//                                HouseHold.dialogRegion.show(modalViewLayout);
//                                resView.model = response;
//                                resView.model.ResLabel = "Creating a new house success was ";
//                                modalViewLayout.centerRegion.show(resView);
//
//                                //Update the member list
//                                if (response.attributes.Success === true) {
//                                    $(".js-logout").show();
//                                    HouseHold.trigger("parent:house");
//                                }
//                            });
//
//                        });
//                    });

                    HouseHold.mainRegion.show(loginLayout);
                });
            }
        };
    });
    return HouseHold.Page.Login.Controller;
});