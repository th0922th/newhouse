/**
 * Created by taylor.hakes on 6/12/14.
 */
'use strict';
define([
    'app',
    'common/views',
    'tpl!apps/login/templates/layout.tpl',
    'tpl!apps/login/templates/login-form.tpl',
    'tpl!apps/login/templates/login-control.tpl',
    'tpl!apps/login/templates/new-house-form.tpl',
    'tpl!apps/login/templates/error.tpl',
    'backbone.syphon'
], function (HouseHold, CommonViews, layoutTpl, loginFormTpl, loginControl, newHouseTpl, errorTpl) {
    HouseHold.module('Reconcile.Transactions.View', function (View, HouseHold, Backbone, Marionette, $, _) {
        View.Layout = Marionette.LayoutView.extend({
            template: layoutTpl,
            regions: {
                formRegion: '#form-region',
                controlRegion: '#control-region',
                errorRegion: '#error-region'
            }
        });

        View.LoginForm = Marionette.ItemView.extend({
            template: loginFormTpl,
            className: 'login-form',
            events: {
                'click .js-submit-button': 'submitClicked'
            },
            submitClicked: function(e){
                e.preventDefault();
                var userData = Backbone.Syphon.serialize(this);
                this.trigger('submit', userData);
            }
        });

        View.LoginControl = Marionette.ItemView.extend({
            template: loginControl,
            className: 'text-right',
            triggers: {
                "click .js-new-house": "new:house"
            }
        });

        View.ErrorResponse = Marionette.ItemView.extend({
            template: errorTpl,
            serializeData: function(){
                return {
                    "ErrorMessage": this.ErrorMessage
                }
            }
        });

        View.NewHouse = Marionette.ItemView.extend({
            template: newHouseTpl
        });

        View.LoginFooter = CommonViews.Footer.extend({
            triggers: {
                "click .js-action": "login:action"
            }
        });
    });
    return HouseHold.Reconcile.Transactions.View;
});