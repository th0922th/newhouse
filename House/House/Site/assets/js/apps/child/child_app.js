/**
 * Created by taylor.hakes on 8/20/2014.
 */
'use strict';
define(['app'], function (HouseHold) {
    HouseHold.module('Child', function (Child) {
        Child.startWithParent = false;
        Child.onStart = function () {
            console.log('Starting Child App.');
        };
        Child.onStop = function () {
            console.log('Stopping Child App.');
        };
    });
    HouseHold.module('Routers.Child', function (ChildRouter, HouseHold, Backbone, Marionette, $, _) {
        ChildRouter.Router = Marionette.AppRouter.extend({
            appRoutes: {
                'child': 'showChild'
            }
        });
        var API = {
            showChild: function (childUser) {
                require([
                    'apps/child/child_controller',
                    'entities/parent'
                ], function (ChildController) {
                    if (childUser != undefined) {
                        ChildController.showChildChores(childUser);
                    }
                    else {
                        HouseHold.request('get:user', function (user) {
                            if (user.attributes.ValidUser === true) {
                                ChildController.showChildChores(user.attributes);
                            }
                            else {
                                HouseHold.trigger('login');
                            }
                        });
                    }
                });
            }
        };

        HouseHold.on('child:house', function (childUser) {
            HouseHold.navigate('child');
            API.showChild(childUser);
        });

        HouseHold.addInitializer(function () {
            new ChildRouter.Router({ controller: API });
        });
    });
    return HouseHold.Child;
});