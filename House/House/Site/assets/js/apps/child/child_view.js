/**
 * Created by taylor.hakes on 8/20/2014.
 */
'use strict';
define([
    'app',
    'common/views',
    'tpl!apps/child/templates/layout.tpl',
    'tpl!apps/child/templates/child-control.tpl',
    'tpl!apps/child/templates/child-chore.tpl',
    'tpl!apps/child/templates/child-chores.tpl',
    'tpl!apps/child/templates/child-payment.tpl',
    'tpl!apps/child/templates/child-payments.tpl',
    "common/helpers",
    'backbone.stickit',
    'backbone.syphon',
    "bootstrap-checkbox",
    'jquery.formatCurrency',
    'bootstrap-select'
], function (HouseHold, CommonViews, layoutTpl, childControlTpl, childChoreTpl, childChoresTpl, childPaymentTpl, childPaymentsTpl) {

    HouseHold.module('Child.View', function (View, HouseHold, Backbone, Marionette, $, _) {
        View.ChildChoreLayout = Marionette.Layout.extend({
            template: layoutTpl,
            regions: {
                childControlRegion: '#child-control-region',
                childPaymentRegion: '#child-payments-region',
                childChoresRegion: '#child-chores-region'
            }
        });

        View.ChildControl = Marionette.ItemView.extend({
            template: childControlTpl,
            triggers: {
                "click": "save:chores"
            }
        });

        View.ChildChorePayment = Marionette.ItemView.extend({
            template: childPaymentTpl,
            tagName: 'tr',
            onBeforeRender: function(){
                var pDate = new Date(parseInt(this.model.attributes.PaidDate.substring(6)));
                this.model.attributes.PaidDate = new String(pDate).toShortDate();
            },
            onRender: function(){
                this.$el.find('.currency').formatCurrency();
            }
        });

        View.ChildChorePayments = Marionette.CompositeView.extend({
            template: childPaymentsTpl,
            childView: View.ChildChorePayment,
            childViewContainer: 'tbody'
        });

        View.ChildChore = Marionette.ItemView.extend({
            template: childChoreTpl,
            tagName: 'tr',
            bindings: {
                '.checkbox': 'CompleteCheck'
            },
            onRender: function(){
                this.stickit();
                this.$el.find('.currency').formatCurrency();
            },
            onBeforeRender: function(){
                var pDate = new Date(parseInt(this.model.attributes.DateAssigned.substring(6)));
                this.model.attributes.DateAssigned = new String(pDate).toShortDate();
            },
            onShow: function(){
                this.$el.find('.checkbox').checkbox({
                    buttonStyle: 'btn-danger',
                    buttonStyleChecked: 'btn-success',
                    checkedClass: 'cb-icon-check',
                    uncheckedClass: 'cb-icon-check-empty'
                });
            }
        });

        View.ChildChores = Marionette.CompositeView.extend({
            template: childChoresTpl,
            tagName: 'table',
            className: 'table table-hover table-striped',
            childView: View.ChildChore,
            childViewContainer: 'tbody'
        });
    });

    return HouseHold.Child.View;
});
