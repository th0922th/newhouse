/**
 * Created by taylor.hakes on 8/20/2014.
 */
'use strict';
define([
    'app',
    'apps/child/child_view',
    'common/views',
    'entities/parent',
    'bootstrap-select'
], function (HouseHold, View, CommonView) {
    HouseHold.module('House.Child', function (Child, HouseHold, Backbone, Marionette, $, _) {
        Child.Controller = {
            showChildChores: function (user) {
                require(['entities/parent'], function () {
                    var childLayoutView = new View.ChildChoreLayout(),
                        childControlView = new View.ChildControl(),
                        childChorePayView = new View.ChildChorePayments(),
                        childChoresView = new View.ChildChores();

                    childLayoutView.on('show', function () {
                        if (user.ValidUser === true) {
                            HouseHold.request('get:member:chores', user.Id, false, function (chores) {
                                childChoresView.collection = chores;
                                childLayoutView.childChoresRegion.show(childChoresView);
                                childLayoutView.childControlRegion.show(childControlView);
                                HouseHold.request('get:paid:chores', user.Id, function(payments){
                                    childChorePayView.collection = payments;
                                    childLayoutView.childPaymentRegion.show(childChorePayView);
                                });
                            });
                        }
                        else {
                            HouseHold.trigger('login');
                        }
                    });

                    childControlView.on('save:chores', function(){
                        var chores = {"MemberChoresReq": []};
                        _.each(childChoresView.collection.models, function(value, key, all){
                            chores.MemberChoresReq.push(value.attributes);
                        });

                        HouseHold.request('save:member:chores', chores, function (res){
                            if(res.attributes[0].Success === true){
                                _.each(childChoresView.collection.models, function(value, key, all){
                                    if(value.attributes.CompleteCheck === true){
                                        childChoresView.collection.models[key].destroy();
                                    }
                                });
                            }
                        });
                    });

                    childChoresView.on('childview:chore:done', function(view, assignMembers){

                    });

                    HouseHold.mainRegion.show(childLayoutView);
                });
            }
        };
    });
    return HouseHold.House.Child.Controller;
});