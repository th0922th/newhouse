'use strict';
define(
    ['app'], function (HouseHold) {
        HouseHold.module('HeaderApp', function (Header, HouseHold, Backbone, Marionette, $, _) {
            var API = {
                listHeader: function () {
                    require(['apps/header/list/list_controller'], function (ListController) {
                        ListController.listHeader();
                    });
                }
            };
            HouseHold.commands.setHandler('set:active:header', function (name) {
                require(['apps/header/list/list_controller'], function (ListController) {
                    ListController.setActiveHeader(name);
                });
            });
            Header.on('start', function () {
                API.listHeader();
            });
        });
        return HouseHold.HeaderApp;
    });