<div class="container">
    <div class="content-wrapper col-xs-12">
        <div class="col-xs-2">
            <p id="logo" class="site-title">
                <a class="logo" href="#"></a>
            </p>
        </div>

        <div class="col-xs-offset-9 col-xs-1">
            <button class="btn btn-default logout-y js-logout">Logout</button>
        </div>
    </div>

    <div class="navbar navbar-inverse col-xs-12">
        <div class="navbar-collapse collapse">
            <ul id="navigation" class="nav navbar-nav"></ul>
        </div>
    </div>
</div>