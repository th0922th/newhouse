'use strict';
define([
    'app',
    'apps/header/list/list_view'
], function (HouseHold, View) {
    HouseHold.module('HeaderApp.List', function (List, HouseHold, Backbone, Marionette, $, _) {
        List.Controller = {
            listHeader: function () {
                require(['entities/header'], function () {
                    //				var links = StoresPaid.request("header:entities");
                    //				var headers = new View.Headers({ collection: links });
                    //				headers.on("brand:clicked", function(){
                    //                    console.log("brand:clicked");
                    //                    Hg.trigger("returns:search");
                    //				});
                    //
                    //				headers.on("itemview:navigate", function(childView, model){
                    //					var trigger = model.get("navigationTrigger");
                    //					console.log("itemview:navigate");
                    //                    Hg.trigger(trigger);
                    //				});
                    var headers = new View.Headers();
                    HouseHold.headerRegion.show(headers);
                });
            },
            setActiveHeader: function (headerUrl) {
            }
        };
    });
    return HouseHold.HeaderApp.List.Controller;
});