'use strict';
define([
    'app',
    'tpl!apps/header/list/templates/header.tpl',
    'tpl!apps/header/list/templates/link.tpl'
], function (HouseHold, headerTpl, headerItemTpl) {
    HouseHold.module('HeaderApp.List.View', function (View, HouseHold, Backbone, Marionette, $, _) {
        View.Header = Marionette.ItemView.extend({
            template: headerItemTpl,
            tagName: 'li',
            events: { 'click a': 'navigate' },
            navigate: function (e) {
                e.preventDefault();
                this.trigger('navigate', this.model);
            },
            onRender: function () {
                if (this.model.selected) {
                    // add class so Bootstrap will highlight the active entry in the navbar
                    this.$el.addClass('active');
                }
            }
        });
        View.Headers = Marionette.CompositeView.extend({
            template: headerTpl,
            childView: View.Header,
            childViewContainer: 'ul',
            events: {
                "click .js-logout": "logout"
            },
            logout: function(e){
                e.preventDefault();
                HouseHold.trigger('logout');
            }
        });
    });
    return HouseHold.HeaderApp.List.View;
});