'use strict';
define([
    'marionette',
    'bootstrap'
], function (Marionette) {
    Marionette.Region.Dialog = Marionette.Region.extend({
        onShow: function (view) {
            view.on('close', this.closeDialog, this);
            this.$el.modal('show');
        },
        closeDialog: function () {
            this.$el.modal('hide');
        }
    });
    return Marionette.Region.Dialog;
});