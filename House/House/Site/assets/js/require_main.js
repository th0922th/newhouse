/**
 * Created by taylor.hakes on 7/1/14.
 */
'use strict';
requirejs.config({
    baseurl: 'assets/js',
    paths: {
        backbone: 'vendor/backbone',
        jquery: 'vendor/jquery',
        json2: 'vendor/json2',
        tpl: 'vendor/tpl',
        underscore: 'vendor/underscore',
        marionette: 'vendor/backbone.marionette',
        'backbone.syphon': 'vendor/backbone.syphon',
        'backbone.picky': 'vendor/backbone.picky',
        'backbone.stickit': 'vendor/backbone.stickit',
        //spin: 'vendor/spin',
        //'spin.jquery': 'vendor/spin.jquery',
        bootstrap: 'vendor/bootstrap/js/bootstrap.min',
        'bootstrap-checkbox': 'vendor/bootstrap-checkbox/js/bootstrap-checkbox',
        'bootstrap-select': 'vendor/bootstrap-select/js/bootstrap-select.min',
        'bootstrap-datetimepicker': 'vendor/bootstrap-datepicker/js/bootstrap-datepicker',
        'bootstrap-phone': 'vendor/bootstrap-formhelpers-phone',
        'backbone.basicauth': 'vendor/backbone.basicauth',
        //moment: 'vendor/moment.min'
        //'jquery.formatCurrency': 'vendor/jquery.formatCurrency'
//        'socket.io': 'vendor/socket.io'
    },
    shim: {
        underscore: { exports: '_' },
        backbone: {
            deps: [
                'jquery',
                'underscore',
                'json2'
            ],
            exports: 'Backbone'
        },
        marionette: {
            deps: [
                'jquery',
                'underscore',
                'backbone'
            ],
            exports: 'Marionette'
        },
        localstorage: ['backbone'],
        'backbone.syphon': ['backbone'],
        'backbone.picky': ['backbone'],
        'backbone.stickit': ['backbone'],
        'backbone.basicauth': ['backbone'],
//        'spin.jquery': [
//            'spin',
//            'jquery'
//        ],
        //moment: ['jquery'],
        bootstrap: ['jquery'],
        'bootstrap-checkbox': ['bootstrap'],
        'bootstrap-select': ['bootstrap'],
        'bootstrap-datetimepicker': ['bootstrap'],
        'bootstrap-phone': ['bootstrap']
        //'jquery.formatCurrency': ['jquery']
    }
});
require([
    'app',
    'apps/header/header_app'
], function (HouseHold) {
    HouseHold.start();
});