/**
 * Created by taylor.hakes on 7/2/14.
 */
'use strict';
define(['app'], function (HouseHold) {
    HouseHold.module('Entities', function (Entities, HouseHold, Backbone, Marionette, $, _) {

        Entities.User = Backbone.Model.extend({
            urlRoot: '/api/home/user'
        });

        Entities.AMember = Backbone.Model.extend({
            urlRoot: '/api/home/members',
            defaults: {
                "Id": 0,
                "Name": "",
                "Password": "",
                "Confirm-Password": "",
                "MemberTypeId": 0,
                "HouseHoldId": 0
            }
        });

        Entities.Members = Backbone.Collection.extend({
//            initialize: function (options) {
//                if (options && options.id) {
//                    this.url = "/api/home/members/" + options.id;
//                }
//            },
            model: Entities.AMember
        });

        Entities.MemberChore = Backbone.Model.extend({
            urlRoot: '/api/home/member/chores',
            defaults: {
                "PaidDate": null
            }
        });

        Entities.MemberChores = Backbone.Collection.extend({
            model: Entities.MemberChore
        });

        Entities.HouseChore = Backbone.Model.extend({
            urlRoot: '/api/home/chores',
            defaults: {
                Id: 0,
                HouseHoldId: 0,
                Name: "",
                Description: "",
                PayRateLow: "",
                PayRateHigh: "",
                DateAssigned: ""
            }
        });

        Entities.HouseChores = Backbone.Collection.extend({
            model: Entities.HouseChore
        });

        var API = {
            getParentMembers: function(user, callback){
                var parentMembers = new Entities.Members();
                parentMembers.url = "/api/home/members/" + user.HouseHoldId;
                var defer = $.Deferred();
                parentMembers.fetch({
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (valiUser) {
                    if (callback !== undefined) {
                        callback(valiUser);
                    }
                });
                return promise;
            },
            getUser: function(callback){
                var user = new Entities.User();
                var defer = $.Deferred();
                user.fetch({
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (user) {
                    if (callback !== undefined) {
                        callback(user);
                    }
                });
                return promise;
            },
            saveMember: function(member, callback){
                var theMember = new Entities.AMember();
                var defer = $.Deferred();
                theMember.save(member, {
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (res) {
                    if (callback !== undefined) {
                        callback(res);
                    }
                });
                return promise;
            },
            updateMember: function(member, callback){
                var theMember = new Entities.AMember();
                theMember.url = "/api/edit/member";
                var defer = $.Deferred();
                theMember.save(member, {
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (res) {
                    if (callback !== undefined) {
                        callback(res);
                    }
                });
                return promise;
            },
            deleteMember: function(id, callback){
                var member = new Entities.AMember();
                member.urlRoot = '/api/home/member/delete/' + id;
                var defer = $.Deferred();
                member.fetch({
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (valiUser) {
                    if (callback !== undefined) {
                        callback(valiUser);
                    }
                });
                return promise;
            },
            deleteMemberChore: function(id, callback){
                var memberChore = new Entities.MemberChore();
                memberChore.urlRoot = '/api/home/member/chore/delete/' + id;
                var defer = $.Deferred();
                memberChore.fetch({
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (valiUser) {
                    if (callback !== undefined) {
                        callback(valiUser);
                    }
                });
                return promise;
            },
            getMemberChores: function(memberId, completed, callback){
                var memChores = new Entities.MemberChores();
                memChores.url = "/api/home/member/chores/" + memberId + '/' + completed;
                var defer = $.Deferred();
                memChores.fetch({
                    success: function (chores) {
                        defer.resolve(chores);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (chores) {
                    if (callback !== undefined) {
                        callback(chores);
                    }
                });
                return promise;
            },
            saveMemberChores: function(chores, callback){
                var memChoresSave = new Entities.MemberChore();
                memChoresSave.url = "/api/home/save/member/chores";
                var defer = $.Deferred();
                memChoresSave.save(chores, {
                    success: function (res) {
                        defer.resolve(res);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (res) {
                    if (callback !== undefined) {
                        callback(res);
                    }
                });
                return promise;
            },
            getHouseChores: function(houseId, callback){
                var houseChores = new Entities.HouseChores();
                houseChores.url = "/api/home/chores/" + houseId;
                var defer = $.Deferred();
                houseChores.fetch({
                    success: function (chores) {
                        defer.resolve(chores);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (chores) {
                    if (callback !== undefined) {
                        callback(chores);
                    }
                });
                return promise;
            },
            saveChore: function(chore, callback){
                var theChore = new Entities.HouseChore();
                theChore.urlRoot = '/api/home/save/chore';
                var defer = $.Deferred();
                theChore.save(chore, {
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (res) {
                    if (callback !== undefined) {
                        callback(res);
                    }
                });
                return promise;
            },
            deleteChore: function(id, callback){
                var chore = new Entities.HouseChore();
                chore.urlRoot = '/api/home/chore/delete/' + id;
                var defer = $.Deferred();
                chore.fetch({
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (valiUser) {
                    if (callback !== undefined) {
                        callback(valiUser);
                    }
                });
                return promise;
            },
            getPaidChores: function(id, callback){
                var paidChores = new Entities.MemberChores();
                paidChores.url = "/api/chores/paid/" + id;
                var defer = $.Deferred();
                paidChores.fetch({
                    success: function (chores) {
                        defer.resolve(chores);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (chores) {
                    if (callback !== undefined) {
                        callback(chores);
                    }
                });
                return promise;
            }
        };

        HouseHold.reqres.setHandler('get:parent:members', function (user, callback) {
            return API.getParentMembers(user, callback);
        });

        HouseHold.reqres.setHandler('get:user', function(callback){
            return API.getUser(callback);
        });

        HouseHold.reqres.setHandler('new:member', function(){
            return new Entities.AMember();
        });

        HouseHold.reqres.setHandler('save:member', function(data, callback){
            return API.saveMember(data, callback);
        });

        HouseHold.reqres.setHandler('update:member', function(data, callback){
            return API.updateMember(data, callback);
        });

        HouseHold.reqres.setHandler('delete:member', function(id, callback){
            return API.deleteMember(id, callback);
        });

        HouseHold.reqres.setHandler('delete:member:chore', function(id, callback){
            return API.deleteMemberChore(id, callback);
        });

        HouseHold.reqres.setHandler('get:member:chores', function(memberId, completed, callback){
            return API.getMemberChores(memberId, completed, callback);
        });

        HouseHold.reqres.setHandler('save:member:chores', function(chores, callback){
            return API.saveMemberChores(chores, callback);
        });

        HouseHold.reqres.setHandler('get:house:chores', function(houseId, callback){
            return API.getHouseChores(houseId, callback);
        });

        HouseHold.reqres.setHandler('new:chore', function(){
            return new Entities.HouseChore();
        });

        HouseHold.reqres.setHandler('save:chore', function(chore, callback){
            return API.saveChore(chore, callback);
        });

        HouseHold.reqres.setHandler('delete:chore', function(id, callback){
            return API.deleteChore(id, callback);
        });

        HouseHold.reqres.setHandler('get:paid:chores', function(id, callback){
            return API.getPaidChores(id, callback);
        });
    });
});