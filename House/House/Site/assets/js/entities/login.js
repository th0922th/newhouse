/**
 * Created by taylor.hakes on 6/12/14.
 */
'use strict';
define(['app'], function (HouseHold) {
    HouseHold.module('Entities', function (Entities, HouseHold, Backbone, Marionette, $, _) {

        Entities.Login = Backbone.Model.extend({
            urlRoot: '/api/login',
            defaults: {
                'username': 'username',
                'password': 'password'
            }
        });

        Entities.Logout = Backbone.Model.extend({url: '/api/logout'});

        Entities.House = Backbone.Model.extend({
            urlRoot: '/api/home/new',
            defaults: {
                "Id":0,
                "HouseName":"",
                "MemberTypeId":1,
                "Name":"",
                "Password":""
            }
        });

        var API = {
            validateLogin: function(user, callback){
                var valiLogin = new Entities.Login();
                var defer = $.Deferred();
                valiLogin.save(user, {
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (valiUser) {
                    if (callback !== undefined) {
                        callback(valiUser);
                    }
                });
                return promise;
            },
            saveHouse: function(data, callback){
                var house = new Entities.House();
                var defer = $.Deferred();
                house.save(data, {
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (valiUser) {
                    if (callback !== undefined) {
                        callback(valiUser);
                    }
                });
                return promise;
            },
            logout: function(callback){
                var logout = new Entities.Logout();
                var defer = $.Deferred();
                logout.fetch({
                    success: function (model) {
                        defer.resolve(model);
                    },
                    error: function () {
                        defer.resolve(undefined);
                    }
                });
                var promise = defer.promise();
                $.when(promise).done(function (res) {
                    if (callback !== undefined) {
                        callback(res);
                    }
                });
                return promise;
            }
        };

        HouseHold.reqres.setHandler('new:login', function () {
            return new Entities.Login();
        });

        HouseHold.reqres.setHandler('validate:login', function (user, callback) {
            return API.validateLogin(user, callback);
        });

        HouseHold.reqres.setHandler('logout', function(callback){
            return API.logout(callback);
        });

        HouseHold.reqres.setHandler('new:house', function(){
            return new Entities.House();
        });

        HouseHold.reqres.setHandler('save:new:house', function(data, callback){
            return API.saveHouse(data, callback);
        });
    });
});