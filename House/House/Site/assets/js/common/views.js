'use strict';
define([
    'app',
    'tpl!common/templates/spin.tpl',
    'tpl!common/templates/bar.tpl',
    'tpl!common/templates/modalLayout.tpl',
    'tpl!common/templates/res.tpl',
    'tpl!common/templates/footer.tpl'//,
    //'spin.jquery'
], function (HouseHold, spinTpl, barTpl, modalLayoutTpl, resTpl, footerTpl) {
    HouseHold.module('Common.Views', function (Views, HouseHold, Backbone, Marionette, $, _) {
        Views.Spin = Marionette.ItemView.extend({
            template: spinTpl,
            initialize: function (opt) {
                var options = opt || {};
                this.title = options.title || 'Loading Data';
                this.message = options.message || 'Please wait, data is loading.';
            },
            serializeData: function () {
                return {
                    title: this.title,
                    message: this.message
                };
            },
            onShow: function () {
                var opts = {
                        lines: 13,
                        length: 20,
                        width: 10,
                        radius: 30,
                        corners: 1,
                        rotate: 0,
                        direction: 1,
                        color: '#000',
                        speed: 1,
                        trail: 60,
                        shadow: false,
                        hwaccel: false,
                        className: 'spinner',
                        zIndex: 2000000000,
                        top: '30px',
                        left: 'auto'
                    };
                //$('#spinner').spin(opts);
            }
        });
        Views.Bar = Marionette.ItemView.extend({
            template: barTpl,
            addClass: 'bar-container'
        });
        Views.ModalLayout = Marionette.LayoutView.extend({
            template: modalLayoutTpl,
            className: "modal-dialog modal-centered",
            regions: {
                centerRegion: '#center-region',
                footerRegion: '#footer-region'
            },
            serializeData: function () {
                return {
                    ModalHeader: this.ModalHeader
                }
            }
        });
        Views.Response = Marionette.ItemView.extend({
            template: resTpl,
            serializeData: function(){
                return{
                    ResLabel: this.model.ResLabel,
                    Success: this.model.attributes.Success,
                    Message: this.model.attributes.Message
                }
            }
        });

        Views.Footer = Marionette.ItemView.extend({
            template: footerTpl,
            serializeData: function(){
                return {
                    Action: this.Action
                }
            }
        });
    });
    return HouseHold.Common.Views;
});