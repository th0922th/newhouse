<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title"><%= ModalHeader %></h3>
    </div>
    <div class="container modal-body" style="width: auto;">
        <div id="center-region"></div>
    </div>
    <div class="modal-footer">
        <div id="footer-region"></div>
    </div>
</div>