/**
 * Created by taylor.hakes on 7/22/2015.
 */
'use strict';

Number.prototype.formatMoney = function (symbol, decplaces, decpt, thous) {
    var n = this,
        c = isNaN(decplaces = Math.abs(decplaces)) ? 2 : decplaces,
        d = decpt == undefined ? "." : decpt,
        t = thous == undefined ? "," : thous,
        isNeg = n < 0 ? (Math.abs(this) < 0.0001 ? false : true) : false,
        s1 = isNeg ? "(" : "",
        s2 = isNeg ? ")" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0,
        x = symbol != undefined ? symbol : '';
    return s1 + x + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + s2;
};