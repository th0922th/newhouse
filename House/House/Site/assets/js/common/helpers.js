define(["app"],
    function (HouseHold) {

        HouseHold.module("Helper", function (Helper, HouseHold, Backbone, Marionette, $, _) {

            Helper.ShortMyText = function(text, length){
                return $.trim(text).substring(0, length).split(" ").slice(0, -1).join(" ") + "...";
            };

            Helper.GetDate = function(){
                var myDate = new Date();
                return (myDate.getMonth()+1) + '/' + (myDate.getDate()) + '/' + myDate.getFullYear();
            }

            String.prototype.toShortDate = function () {

                if (typeof (this) === 'undefined') return '';


                return new Date(this).toShortDate();

            }

            Date.prototype.toShortDate = function () {

                var month = this.getMonth() + 1;

                if (month < 10)

                    month = '0' + month;


                var date = this.getDate();

                if (date < 10)

                    date = '0' + date;


                return month + '/' + date + '/' + this.getFullYear();

            }

        });

        return HouseHold.Helper;

    });