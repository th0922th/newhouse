/**
 * Created by taylor.hakes on 6/29/14.
 */
var _ = require('underscore'),
    nconf = require('../config/config'),
    jsonRequest = require('request-json'),
    houseHoldUrl = nconf.get('api:houseHoldUrl'),
    client = jsonRequest.newClient(houseHoldUrl),
    houseHoldApi = {};

function handleCallback(error, res, callback) {
    if (error) {
        throw error;
    }

    if (callback !== 'undefined') {
        callback(res);
    }
}

houseHoldApi.login = function (req, callback) {
    console.log("API Login");
    handleCallback(null, req.user, callback);
};

houseHoldApi.user = function(req, callback){
    handleCallback(null, req.user, callback)
};

houseHoldApi.fail = function(req, callback){
    handleCallback(null, {"Success": false}, callback)
};

houseHoldApi.getMembers = function(id, callback){
    client.get('GetMembers/' + id, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.saveMember = function(req, callback){
    client.post('SaveMember', req.body, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback)
        }
        else {
            handleCallback(err, null, callback)
        }
    });
};

houseHoldApi.editMember = function(req, callback){
    client.put('UpdateMember', req.body, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback)
        }
        else {
            handleCallback(err, null, callback)
        }
    });
};

houseHoldApi.deleteMember = function(id, callback){
    client.get('DeleteMember/' + id, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.deleteMemberChore = function(id, callback){
    client.get('DeleteMemberChore/' + id, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.deleteChore = function(id, callback){
    client.get('DeleteChore/' + id, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.getMemberChores = function(id, completed, callback){
    client.get('GetMemberChores/' + id + '/' + completed, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.getPaidChores = function(id, callback){
    client.get('GetPaidChores/' + id, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.saveMemberChores = function(chores, callback){
    client.post('SaveMemberChores', chores, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.getHouseChores = function(id, callback){
    client.get('GetHouseChores/' + id, function (err, res, body) {
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.saveChore = function(chore, callback){
    client.post('SaveChore', chore, function(err, res, body){
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

houseHoldApi.saveHouse = function(house, callback){
    client.post('SaveHouse', house, function(err, res, body){
        if (!err) {
            handleCallback(null, body, callback);
        }
        else {
            handleCallback(err, null, callback);
        }
    });
};

module.exports = houseHoldApi;