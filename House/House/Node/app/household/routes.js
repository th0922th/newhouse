'use strict';
module.exports = function (app, injector) {
    var api = injector.get('houseHoldApi'),
        db = injector.get('myConnection'),
        passport = require('passport'),
        path = require('path'),
        auth = require(path.join(__dirname, '../auth/index'));

    app.post('/api/login',
        passport.authenticate('local', { failureRedirect: '/api/home/fail', failureFlash: false }),
        function (req, res) {
            console.log("Routes Login");
            res.send(req.user);
        });

    app.get('/api/logout', function (req, res, next) {
        req.logout();
        res.send({"Success": true});
    });

    app.get('/api/home/user', auth.ensureAuthenticated, function (req, res) {
        res.send(req.user);
    });

    app.get('/api/home/fail', function (req, res) {
        api.fail(req, function (result) {
            res.send(result);
        });
    });

    //app.get('/', function(req, res){
//		    res.send("Hello from test1.");
//		    });

    app.get('/api/home/members/:id', function (req, res) {
        var id = req.params.id;
        db.query('SELECT m.Id, m.MemberTypeId, m.HouseHoldId, m.Name, m.Password FROM House.Member m WHERE m.HouseHoldId = ' + id, function (err, rows, fields) {
            if (!err) {
                res.send(rows);
            }
            else {
                console.log('Error getting all members for house with houseId of %i.', id);
                res.send(err);
            }
        });
    });

    app.post('/api/home/members', function (req, res) {
        var memberTypeId = req.body.MemberTypeId,
            houseId = req.body.HouseHoldId,
            name = req.body.Name,
            password = req.body.Password,
            query = "".concat('Insert Into Member Values (null, ', memberTypeId, ', ', houseId, ', "', name, '", "', password, '" )');

        db.query(query, function (err, rows, fields) {
            if (!err) {
                res.send({id: rows.insertId});
            }
            else {
                res.send(err);
            }
        });

    });

    app.post('/api/edit/member', function (req, res) {
        var memberId = req.body.MemberId,
            memberTypeId = req.body.MemberTypeId,
            houseId = req.body.HouseHoldId,
            name = req.body.Name,
            password = req.body.Password,
            query = "".concat("Update Member Set MemberTypeId = ", memberTypeId, ", HouseHoldId = ", houseId, ", Name = '", name, "', Password = '", password, "' Where Id = ", memberId);

        db.query(query, function (err, rows, fields) {
            if (!err) {
                res.send({Success: true});
            }
            else {
                res.send(err);
            }
        });
    });

    app.get('/api/home/member/delete/:id', function (req, res) {
        var id = req.params.id,
            query = "".concat("Delete From Member Where Id = ", id);
        if (id > -1 && id != undefined) {
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    res.send("Deleted Member");
                }
                else {
                    res.send(err);
                }
            });
        }
    });

    app.get('/api/home/member/chore/delete/:id', function (req, res) {
        var id = req.params.id,
            query = "".concat("Delete From Member_Chore Where Id = ", id);
        if (id > -1 && id != undefined) {
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    res.send({'Success': true, 'Message': 'Member chore was successfully deleted.'});
                }
                else {
                    res.send({'Success': false, 'Message': 'Member chore cannot be deleted.'});
                }
            });
        }
    });

    app.get('/api/home/chore/delete/:id', function (req, res) {
        var id = req.params.id,
            query = "".concat("Delete From Chore Where Id = ", id);
        if (id > -1 && id != undefined) {
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    res.send({'Success': true, 'Message': 'Chore was successfully deleted.'});
                }
                else {
                    res.send({'Success': false, 'Message': 'Chore cannot be deleted.'});
                }
            });
        }
    });

    app.get('/api/home/member/chores/:id/:completed', function (req, res) {
        console.log("Get member chores!");
        var id = req.params.id,
            completed = req.params.completed,
            query = "".concat("SELECT mc.Id, mc.MemberId, mc.ChoreId, mc.DateAssigned, mc.DateCompleted, mc.CompleteCheck, mc.ConfirmedCheck, mc.FinalEarning, c.Name, c.Description, c.PayRateLow, c.PayRateHigh FROM Member_Chore mc INNER JOIN Chore c ON mc.ChoreId = c.Id WHERE mc.MemberId = ", id, " AND mc.CompleteCheck = ", completed, " AND mc.PaidDate is null");
        if (id > -1 && id != undefined) {
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    res.send(rows);
                }
                else {
                    res.send(err);
                }
            });
        }
    });

    app.get('/api/chores/paid/:id', function (req, res) {
        var id = req.params.id,
            query = "".concat("SELECT mc.PaidDate as PaidDate, sum(mc.FinalEarning) as FinalEarning FROM Member_Chore mc INNER JOIN Chore c ON mc.ChoreId = c.Id WHERE mc.MemberId = ", id, " AND mc.PaidDate is not null group by mc.PaidDate");
        if (id > -1 && id != undefined) {
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    res.send(rows);
                }
                else {
                    res.send(err);
                }
            });
        }
    });

    app.post('/api/home/save/member/chores', function (req, res) {
        var mcs = req.body.MemberChoresReq, query;

        function Success(pass) {
            if (pass === true) {
                console.log("Setting succes true");
                mcs[0].Success = true;
            }
            else {
                console.log("Setting succes false");
                mcs[0].Success = false;
            }
        }

        //Update or Save?
        for (var i = 0; i < mcs.length; i++) {
            if (mcs[i].Id != undefined && mcs[i].Id > 0) {
                query = "".concat("Update Member_Chore Set CompleteCheck=", mcs[i].CompleteCheck, ",DateCompleted='", mcs[i].DateCompleted, "',FinalEarning=", mcs[i].FinalEarning, ",ConfirmedCheck=", mcs[i].ConfirmedCheck, ",PaidDate=", mcs[i].PaidDate);
            }
            else {
                query = "".concat("Insert Into Member_Chore Values (null,", mcs[i].MemberId, ",", mcs[i].ChoreId, ",STR_TO_DATE('", mcs[i].DateAssigned, "', '%Y-%m-%d')", ",", mcs[i].DateCompleted, ",", mcs[i].CompleteCheck, ",", mcs[i].FinalEarning, ",", mcs[i].ConfirmedCheck, ",", mcs[i].PaidDate, ")");
            }
            console.log("Query");
            console.log(query);
            console.log(mcs[i]);
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    console.log("No DB Error");
                    console.log(rows);
                    Success(true);
                }
                else {
                    console.log("DB Error");
                    console.log(err);
                    Success(false);
                }
            });
            if (i == mcs.length) {
                console.log("Sending mcs");
                res.send(mcs);
            }
        }
    });

    app.get('/api/home/chores/:id', function (req, res) {
        var id = req.params.id;
        if (id > -1 && id != undefined) {
            var query = "".concat("Select * From Chore Where HouseHoldId = ", id);
            db.query(query, function (err, rows, fields) {
                if (!err) {
                    res.send(rows);
                }
                else {
                    res.send(err);
                }
            });
        }
    });

    app.post('/api/home/save/chore', function (req, res) {
        console.log("Save/Update Chore");
        var chore = req.body,
            query = "".concat("Insert Into Chore Values (null,", chore.HouseHoldId, ",'", chore.Name, "','", chore.Description, "',", chore.PayRateLow, ",", chore.PayRateHigh, ")");

        if (chore.Id != undefined && chore.Id > 0) {
            query = "".concat("Update Chore Set Name='", chore.Name, "',Description='", chore.Description, "',PayRateLow=", chore.PayRateLow, ",PayRateHigh=", chore.PayRateHigh, " Where Id = ", chore.Id);
        }
        console.log(query);
        db.query(query, function (err, rows, fields) {
            if (!err) {
                res.send({'Success': true});
            }
            else {
                res.send(err);
            }
        });
    });

    app.post('/api/home/new', function (req, res) {
        var name = req.body.HouseName,
            query = "".concat("Insert Into HouseHold Values (null,", name, ")");
        db.query(query, function (err, rows, fields) {
            if (!err) {
                res.send(rows);
            }
            else {
                res.send(err);
            }
        });
    });
};
