/**
 * Created by taylor.hakes on 6/29/14.
 */
'use strict';
var application_root = __dirname,
    path = require('path'),
    express = require('express'),
    methodOverride = require('method-override'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    exSession = require('express-session'),
    RedisStore = require('connect-redis')(exSession),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    mysql = require('mysql'),
    app = express(),
    connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'S0phia01!',
        database: 'house'
    }),    
    config = require('./config/app.json'),
    routes = [path.join(application_root, 'household/routes')],
    di = require('di'),
    houseHoldApi = require(path.join(__dirname, 'household/api')),
    dependency = {
        'houseHoldApi': ['type', function(){ return houseHoldApi; }],
        'myConnection': ['type', function(){ return connection;}]
    },
    injector = new di.Injector([dependency]);

var server,
    port;

if(process.env.OPENSHIFT_NODEJS_PORT != undefined){
    console.log("using openshift port.");
    port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
}
else{
    port = config.api.nodePort;
}

passport.serializeUser(function(user, done){
    done(null, user);
});

passport.deserializeUser(function(user, done){
    done(null, user);
});

passport.use(new LocalStrategy(
    function(username, password, done) {
	var query = "".concat("Select * From Member Where Name = '", username, "' and Password = '", password, "'");
	connection.query(query, function(err, rows, fields){
	    if(!err){
         if(rows.length === 1 && rows[0].Id != undefined && rows[0].Id > 0)
         {             
             return done(null, {
                               'ValidUser': true,
                               'Id': rows[0].Id,
                               'HouseHoldId': rows[0].HouseHoldId,
                               'MemberTypeId': rows[0].MemberTypeId,
                               'Name': rows[0].Name
                               });
         }
         return done(null, false);
	    }
	    else{
         console.log("Error finding user:" + err);
         return done(err);
	    }
	});
    }
));

var serverFuncs = {
    start: function(injector){
        //app.set('views', path.join(application_root,  '../../Site'));
        app.use(methodOverride());
        //app.use(express.urlencoded({limit: '50mb'}));
        

        app.use(cookieParser());
        app.use(exSession({
            resave: true,
            saveUninitialized: true,
            //store: new RedisStore({
            //    host: 'localhost',
            //    port: 6379,
            //    ttl: 1800000
            //}),
            secret: '1234567890QWERTY',
            //cookie: {maxAge: 1800000},
            //rolling: true
        }));

        app.use(bodyParser.json({limit: '50mb'}));
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(passport.initialize());
        app.use(passport.session());

        routes.forEach(function( route){
            require(route)(app, injector);
        });
        //app.use(app.router);
        app.use(express.static(path.join(application_root,  '../../Site')));
        //app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));

        server = app.listen(port, function(err){
            if(err){
                console.log(err);
            }
            else{
                console.log('HouseHold server listening on port %d in %s mode', port, app.settings.env);
            }
        });
    },

    stop: function(){
        if(server !== 'undefined'){
            server.close();
        }
    }
};

if(!module.parent){
    serverFuncs.start(injector);
}
else{
    module.exports = serverFuncs;
}

//io.on('connection', function(socket){
//    console.log('A user connected!')
//});
