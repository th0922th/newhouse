/**
 * Created by taylor.hakes on 6/29/14.
 */
'use strict';
var nconf = require('nconf'),
    fs = require('fs'),
    path = require('path');

function settings() {
    var configs = path.join(__dirname, 'app.json');
    if(fs.existsSync(configs))
    {
        nconf.use('file', {file: configs});
    }

    nconf.load();
    return nconf;
}

module.exports = settings();