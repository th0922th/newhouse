/**
 * Created by taylor.hakes on 6/29/14.
 */
'use strict';
var config = {};

config.redisPort = "6379";
config.nodePort = "5500";

module.exports = config;
