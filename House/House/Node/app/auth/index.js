/**
 * Created by taylor.hakes on 7/5/14.
 */
'use strict';

function ensureAuthenticated(req,res,next){
    if (req.isAuthenticated() && req.user.ValidUser === true) { return next(); }
    res.send({"AuthSuccess": false});
}

module.exports = {
    ensureAuthenticated: ensureAuthenticated
};